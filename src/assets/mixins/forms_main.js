import IvyInput from "@/components/form-elements/Input";
import IvySelect from "@/components/form-elements/Select";
import IvyFile from "@/components/form-elements/FileUpload";
import IvyCheckbox from "@/components/form-elements/Checkbox";
import IvyTextarea from "@/components/form-elements/Textarea";
import Autocomplete from "@/components/form-elements/Autocomplete";

export default {
  components: {
    IvyInput,
    IvySelect,
    IvyFile,
    IvyCheckbox,
    IvyTextarea,
    Autocomplete
  },
  mounted() {
    this.validation()
  },
  methods: {
    validation() {
      const forms = document.querySelectorAll('.needs-validation')
      Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.classList.add('was-validated')
        }, false)
      })
    }
  },
}