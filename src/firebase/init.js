// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCphm3pgu7esv7Vn9bIQ4JndBINl6MZycw",
    authDomain: "ivy-exec.firebaseapp.com",
    databaseURL: "https://ivy-exec.firebaseio.com",
    projectId: "ivy-exec",
    storageBucket: "ivy-exec.appspot.com",
    messagingSenderId: "762970043777",
    appId: "1:762970043777:web:63a7dbbc5c175d25ba6480",
    measurementId: "G-2CNH3YG4ZY"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore()

export default firebaseApp.firestore()