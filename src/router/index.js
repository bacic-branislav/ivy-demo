import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/home/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import(/* webpackChunkName: "register" */ '../views/register/Register.vue')
  },
  {
    path: '/signin',
    name: 'Signin',
    component: () => import(/* webpackChunkName: "signin" */ '../views/sign-in/SignIn')
  },
  {
    path: '/employers',
    name: 'Employers',
    component: () => import(/* webpackChunkName: "employers" */ '../views/employers/Employers.vue')
  },
  {
    path: '/classes',
    name: 'Classes',
    component: () => import(/* webpackChunkName: "classes" */ '../views/classes/Classes.vue'),
  },
  {
    path: '/classes/details/:slug',
    name: 'ClassesDetails',
    props: true,
    component: () => import(/* webpackChunkName: "classesDetails" */ '../views/classes/ClassesDetails.vue'),
  },
  {
    path: '/jobs',
    name: 'Jobs',
    props: true,
    component: () => import(/* webpackChunkName: "jobs" */ '../views/jobs/Jobs.vue'),
    children: [
      {
        path: ":title/:location/:id",
        name: "JobDetails",
        props: true,
        component: () => import(/* webpackChunkName: "jobDetails" */ '../views/jobs/JobDetails')
      }
    ]
  },
  {
    path: "/jobs/new",
    name: "New",
    props: true,
    component: () => import(/* webpackChunkName: "jobDetails" */ '../views/jobs/JobNew')
  },
]

const router = createRouter({
  history: createWebHistory(),
  mode: 'history',
  routes
})

export default router
