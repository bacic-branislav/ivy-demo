import {createStore} from 'vuex'
import {COUNTRIES, LOCATIONS} from "@/constants/dropdown-options";

export default createStore({
  state: {
    user: {
      logged: false,
      email: "",
    },
    dark_mode: false,
    countries: COUNTRIES,
    locations: LOCATIONS,
    classes: [],
    jobs: [],
    selectedJobID: ""
  },
  mutations: {
    change_theme(state) {
      state.dark_mode = !this.state.dark_mode
    },
    user_login(state, payload) {
      state.user.logged = true
      state.user.email = payload.email
    },
    user_logout(state) {
      state.user.logged = false
      state.user.email = ""
    },
    get_all_classes(state, payload) {
      state.classes = payload
    },
    get_all_jobs(state, payload) {
      state.jobs = payload
    },
    update_selected_job_id(state, id) {
      state.selectedJobID = id
    }
  },
  actions: {},
  modules: {}
})
