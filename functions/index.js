const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const createNotification = (notification => {
    return admin.firestore().collection('notifications')
        .add(notification)
        .then(doc => console.log('notification added', doc));
})

exports.jobCreated = functions.firestore
    .document('jobs/{jobId}')
    .onCreate(doc => {
        const job = doc.data();
        const notification = {
            header: 'A new job has just been added',
            jobTitle: `${job.job_title}`,
            companyName: `${job.company_name}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createNotification(notification)
    })
